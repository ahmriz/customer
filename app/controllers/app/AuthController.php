<?php

class App_AuthController extends BaseController{
    public function Home(){
        $customer = new Customer();
        $getcustomers = $customer->GetCustomers();
        $dataArray['customers'] = $getcustomers;
        //echo '<pre>';print_r($getcustomers);echo '</pre>';
        
        return View::make('customer.customer_home', $dataArray);
    }
    
    public function New_Customer(){
        $dataArray['result'] = null;
        $dataArray['title'] = "New customer";
        return View::make('customer.customer_new', $dataArray);
    }
    public function Add_Customer(){
        $newArray['title'] = 'Add new customer';
        
        $customer = new Customer();
        $dataArray['name'] = Input::get('name');
        $dataArray['email'] = Input::get('email');
        $dataArray['address'] = Input::get('address');
        $dataArray['phone'] = Input::get('phone');
        $customerid = $customer->AddCustomer($dataArray);
        
        if($customerid){
            $newArray['result'] = "New customer added";
        }else{
            $newArray['result'] = "There was an error";
        }
        
        return View::make('customer.customer_new', $newArray);
                
    }
    
    public function Customer_edit($id){
        $customer = new Customer();
        $findCustomer = $customer->FindCustomer($id);
        $dataArray['id'] = $findCustomer->id;
        $dataArray['name'] = $findCustomer->name;
        $dataArray['email'] = $findCustomer->email;
        $dataArray['address'] = $findCustomer->address;
        $dataArray['phone'] = $findCustomer->phone;
        $dataArray['result'] = null;
        $dataArray['title'] = "Update customer";
        return View::make('customer.customer_edit', $dataArray);
    }
    
    public function Customer_update($id){
        $customer = new Customer();
        $updateArray['id'] = $id;
        $updateArray['name'] = Input::get('name');
        $updateArray['email'] = Input::get('email');
        $updateArray['address'] = Input::get('address');
        $updateArray['phone'] = Input::get('phone');
        //echo $id;
        $update = $customer->UpdateCustomer($updateArray);
        
        if($update){
            echo "updated";
        }else{
            echo 'error';
        }
    }
}

