<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::get('/', function()
//{
//	return View::make('hello');
//});

Route::group(array('prefix' => 'app'), function()
{
    Route::get('auth/home','App_AuthController@Home');
    Route::get('auth/new','App_AuthController@New_Customer');
    
    Route::post('auth/add','App_AuthController@Add_Customer');
    
    Route::get('auth/edit/{id}','App_AuthController@Customer_edit');
    
    Route::post('auth/update/{id}','App_AuthController@Customer_update');
});