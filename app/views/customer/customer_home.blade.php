
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>Show customer details</div>
        <div>{{ HTML::link('app/auth/new', 'Add new Customer')}}</div>
        <table>
            <tr>
                <td>Customer Id</td>
                <td>Customer Name</td>
                <td>Customer Email</td>
                <td>Customer Address</td>
                <td>Customer Phone</td>
                <td>Actions</td>
            </tr>
            <?php foreach($customers as $customer){ ?>
            <tr>
                <td><?php echo $customer->id;?></td>
                <td><?php echo $customer->name;?></td>
                <td><?php echo $customer->email;?></td>
                <td><?php echo $customer->address;?></td>
                <td><?php echo $customer->phone;?></td>
                <td>{{ HTML::link('app/auth/edit/'.$customer->id, 'Edit','class="btn  btn-sm btn-info"')}}</td>
            </tr>
            <?php } ?>
        </table>
    </body>
</html>

