<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Customer extends Eloquent implements UserInterface, RemindableInterface {
        use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customer';
	//protected $primaryKey = 'Ref_ID';
        
        public function AddCustomer($dataArray){
            $this->name  = $dataArray['name'];			
            $this->email = $dataArray['email'];
            $this->address = $dataArray['address'];
            $this->phone = $dataArray['phone'];
            $this->active  = 1;
            $this->created_at  = time();
            $this->updated_at  = 0;

            if($this->save()){
                    $userId = $this->id;
                    return $userId;
            }else{
                    return false;
            }      
        }
    
    
        public function GetCustomers(){
            $allrecords = DB::table('customer')                                                            
                ->where('active',1)
                ->get();

                return $allrecords; 
        }
        public function FindCustomer($id){
            $allrecords = DB::table('customer')  
                ->where('id', '=',$id)    
                ->where('active', '=',1)
                ->first();

                return $allrecords; 
        }
        
        public function UpdateCustomer($dataArray){ 		
		$allData = DB::table('customer')
                ->where('id', $dataArray['id'])
                ->update($dataArray);
            
            return $allData;
    }
}